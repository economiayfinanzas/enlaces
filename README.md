# ENLACES



## Introducción

En este repositorio encontrarás enlaces divididos por categorías referidos a economía y finanzas. A medida que se vayan subiendo enlaces con nuevas categorías estas se irán indicando debajo.

## Categorías


- [Niños y Adolescentes](./AdolescentesYNi%C3%B1os)
- [Trabajo](./Trabajo)
